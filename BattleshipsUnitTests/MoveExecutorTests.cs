﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Battleships;
using Battleships.Structures;
using FluentAssertions;
using NSubstitute;
using Xunit;

namespace BattleshipsUnitTests
{
    public class MoveExecutorTests
    {
        private readonly IShooter _shooter;
        public MoveExecutorTests()
        {
            _shooter = new Shooter();
        }

        [Fact]
        public void Execute_Miss_AddMissShotToBoard()
        {
            var board = Substitute.For<IGameBoard>();
            board.AddMissShot(Arg.Any<Segment>());
            board.UpdateBoard(Arg.Any<List<IShip>>());

            (var finish, var shotResult) = _shooter.Shoot(0, 1, board, new List<IShip>());

            board.Received(1).AddMissShot(Arg.Any<Segment>());
            board.Received(1).UpdateBoard(Arg.Any<List<IShip>>());
            finish.Should().Be(false);
            shotResult.Should().Be(SegmentState.Miss);
        }

        [Fact]
        public void Execute_Hit_ChangeSegmentStateToHit()
        {
            var board = Substitute.For<IGameBoard>();
            board.UpdateBoard(Arg.Any<List<IShip>>());
            var ship = new Ship(4,"Destroyer")
            {
                Segments = new List<Segment>()
                {
                    new Segment(){Row = 0, Column = 0, SegmentState = SegmentState.Unknown},
                    new Segment(){Row = 0, Column = 0, SegmentState = SegmentState.Unknown},
                    new Segment(){Row = 0, Column = 0, SegmentState = SegmentState.Unknown},
                    new Segment(){Row = 0, Column = 0, SegmentState = SegmentState.Unknown}
                }
            };
            var ships = new List<IShip>(){ ship };

            (var finish, var shotResult) = _shooter.Shoot(0, 0, board, ships);

            ship.GetSegment(0, 0).SegmentState.Should().Be(SegmentState.Hit);
            board.Received(1).UpdateBoard(Arg.Any<List<IShip>>());
            finish.Should().Be(false);
            shotResult.Should().Be(SegmentState.Hit);
        }

        [Fact]
        public void Execute_HitAndSunkLastShip_ReturnSegmentStatusSunkAndFinishTrue()
        {
            var board = Substitute.For<IGameBoard>();
            board.UpdateBoard(Arg.Any<List<IShip>>());
            var ship = new Ship(4, "Destroyer")
            {
                Segments = new List<Segment>()
                {
                    new Segment(){Row = 0, Column = 0, SegmentState = SegmentState.Unknown},
                    new Segment(){Row = 0, Column = 1, SegmentState = SegmentState.Hit},
                    new Segment(){Row = 0, Column = 2, SegmentState = SegmentState.Hit},
                    new Segment(){Row = 0, Column = 3, SegmentState = SegmentState.Hit}
                }
            };
            var ships = new List<IShip>() { ship };
            (var finish, var shotResult) = _shooter.Shoot(0, 0, board, ships);

            ship.Segments.All(segment => segment.SegmentState == SegmentState.Sunk).Should().Be(true);
            board.Received(1).UpdateBoard(Arg.Any<List<IShip>>());
            finish.Should().Be(true);
            shotResult.Should().Be(SegmentState.Sunk);
        }

        [Fact]
        public void Execute_HitAndSunkNotLastShip_ReturnSegmentStatusSunkAndFinishFalse()
        {
            var board = Substitute.For<IGameBoard>();
            board.UpdateBoard(Arg.Any<List<IShip>>());
            var ship = new Ship(1, "TestShip")
            {
                Segments = new List<Segment>()
                {
                    new Segment(){Row = 0, Column = 0, SegmentState = SegmentState.Unknown}
                }
            };
            var ship2 = new Ship(1, "TestShip")
            {
                Segments = new List<Segment>()
                {
                    new Segment(){Row = 0, Column = 0, SegmentState = SegmentState.Unknown}
                }
            };
            var ships = new List<IShip>() { ship, ship2 };
            (var finish, var shotResult) = _shooter.Shoot(0, 0, board, ships);

            ship.Segments.All(segment => segment.SegmentState == SegmentState.Sunk).Should().Be(true);
            board.Received(1).UpdateBoard(Arg.Any<List<IShip>>());
            finish.Should().Be(false);
            shotResult.Should().Be(SegmentState.Sunk);
        }
    }
}
