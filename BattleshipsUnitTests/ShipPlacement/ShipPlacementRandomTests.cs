﻿using System;
using System.Collections.Generic;
using System.Linq;
using Battleships;
using Battleships.ShipPlacement;
using Battleships.Structures;
using FluentAssertions;
using NSubstitute;
using Xunit;

namespace BattleshipsUnitTests.ShipPlacement
{
    public class ShipPlacementRandomTests
    {
        [Fact]
        public void FindEmptyPlaceForShip_WithEmptyBoard_ReturnListOfSegments()
        {
            ShipPlacementRandom shipPlacementRandom = new ShipPlacementRandom(new Random());

            var result = shipPlacementRandom.FindEmptyPlaceForShip(4, new List<IShip>());

            result.Count.Should().Be(4);
        }

        [Fact]
        public void FindEmptyPlaceForShip_OrientationVertical00StartSegment_ReturnListOfSegments()
        {
            var random = Substitute.For<Random>();
            random.Next(Arg.Any<int>()).Returns(0, 0, 0);
            ShipPlacementRandom shipPlacementRandom = new ShipPlacementRandom(random);

            var results = shipPlacementRandom.FindEmptyPlaceForShip(4, new List<IShip>());

            results.Count.Should().Be(4);
            results.All(segment => segment.Column == 0 && segment.SegmentState == SegmentState.Unknown).Should().Be(true);
            var orderedResults = results.OrderBy(segment => segment.Row).ToList();
            orderedResults[0].Row.Should().Be(0);
            orderedResults[1].Row.Should().Be(1);
            orderedResults[2].Row.Should().Be(2);
            orderedResults[3].Row.Should().Be(3);
        }

        [Fact]
        public void FindEmptyPlaceForShip_OrientationHorizontal00StartSegment_ReturnListOfSegments()
        {
            var random = Substitute.For<Random>();
            random.Next(Arg.Any<int>()).Returns(1, 0, 0);
            ShipPlacementRandom shipPlacementRandom = new ShipPlacementRandom(random);

            var results = shipPlacementRandom.FindEmptyPlaceForShip(4, new List<IShip>());

            results.Count.Should().Be(4);
            results.All(segment => segment.Row == 0 && segment.SegmentState == SegmentState.Unknown).Should().Be(true);
            var orderedResults = results.OrderBy(segment => segment.Column).ToList();
            orderedResults[0].Column.Should().Be(0);
            orderedResults[1].Column.Should().Be(1);
            orderedResults[2].Column.Should().Be(2);
            orderedResults[3].Column.Should().Be(3);
        }

        [Fact]
        public void FindEmptyPlaceForShip_WithNotEmptyBoard_ReturnListOfSegments()
        {
            var random = Substitute.For<Random>();
            random.Next(Arg.Any<int>()).Returns(1, 0, 0, 0, 0, 7, 1, 1, 1);
            var ship1 = Substitute.For<IShip>();
            ship1.Segments = new List<Segment>()
            {
                new Segment(){Row = 0, Column = 0, SegmentState = SegmentState.Sunk},
                new Segment(){Row = 1, Column = 0, SegmentState = SegmentState.Sunk},
                new Segment(){Row = 2, Column = 0, SegmentState = SegmentState.Sunk},
                new Segment(){Row = 3, Column = 0, SegmentState = SegmentState.Sunk}
            };
            var ship2 = Substitute.For<IShip>();
            ship2.Segments = new List<Segment>()
            {
                new Segment(){Row = 0, Column = 4, SegmentState = SegmentState.Sunk},
                new Segment(){Row = 0, Column = 5, SegmentState = SegmentState.Sunk},
                new Segment(){Row = 0, Column = 6, SegmentState = SegmentState.Sunk},
                new Segment(){Row = 0, Column = 7, SegmentState = SegmentState.Sunk}
            };
            ShipPlacementRandom shipPlacementRandom = new ShipPlacementRandom(random);

            var results = shipPlacementRandom.FindEmptyPlaceForShip(4, new List<IShip>(){ship1, ship2});

            results.Count.Should().Be(4);
            results.All(segment => segment.Row == 1 && segment.SegmentState == SegmentState.Unknown).Should().Be(true);
            var orderedResults = results.OrderBy(segment => segment.Column).ToList();
            orderedResults[0].Column.Should().Be(1);
            orderedResults[1].Column.Should().Be(2);
            orderedResults[2].Column.Should().Be(3);
            orderedResults[3].Column.Should().Be(4);
        }
    }
}
