﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Battleships;
using Battleships.Structures;
using FluentAssertions;
using Xunit;

namespace BattleshipsUnitTests
{
    public class ShipTests
    {
        private readonly IShip _ship;
        private readonly Segment _segment1, _segment2, _segment3, _segment4;
        public ShipTests()
        {
            _segment1 = new Segment() { Row = 0, Column = 0, SegmentState = SegmentState.Unknown };
            _segment2 = new Segment() { Row = 0, Column = 1, SegmentState = SegmentState.Unknown };
            _segment3 = new Segment() { Row = 0, Column = 2, SegmentState = SegmentState.Unknown };
            _segment4 = new Segment() { Row = 0, Column = 3, SegmentState = SegmentState.Unknown };

            _ship = new Ship(4, "Destroyer")
            {
                Segments = new List<Segment>() { _segment1,_segment2,_segment3,_segment4 }
            };
        }

        [Fact]
        public void Ship_WhenCreated_SetNameAndSizeProperties()
        {
            _ship.Name.Should().Be("Destroyer");
            _ship.Size.Should().Be(4);
        }

        [Fact]
        public void TrySink_ForShipWithSegmentsThatAreNotHit_ReturnFalse()
        {
            var result = _ship.TrySink();

            result.Should().Be(false);
            _ship.Segments.All(segment => segment.SegmentState == SegmentState.Unknown);
        }

        [Fact]
        public void TrySink_ForShipWhereAllSegmentsAreHit_ReturnTrueChangeSegmentStatusToSunk()
        {
            _ship.Segments.ForEach(segment => segment.SegmentState=SegmentState.Hit);

            var result = _ship.TrySink();

            result.Should().Be(true);
            _ship.Segments.All(segment => segment.SegmentState == SegmentState.Sunk);
        }

        [Fact]
        public void GetSegment_WhenSegmentExist_ReturnSegment()
        {
            var result  = _ship.GetSegment(0, 0);

            result.Should().Be(_segment1);
        }

        [Fact]
        public void GetSegment_WhenSegmentNotExist_ReturnNull()
        {
            var result = _ship.GetSegment(5, 0);

            result.Should().Be(null);
        }
    }
}
