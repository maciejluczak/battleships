﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Battleships;
using Battleships.Structures;
using Xunit;
using FluentAssertions;
using NSubstitute;

namespace BattleshipsUnitTests
{
    public class GameBoardTests
    {
        private GameBoard _gameBoard;
        public GameBoardTests()
        {
            _gameBoard = new GameBoard();
        }

        [Fact]
        public void GameBoard_WhenCreated_CreateBoardWithUnknowStatusField()
        {
            _gameBoard.Board.Count.Should().Be(100);
            _gameBoard.Board.All(segment => segment.SegmentState == SegmentState.Unknown);
            _gameBoard.Miss.Count.Should().Be(0);
        }

        [Fact]
        public void UpdateBoard_ForGivenListOfShips_UpdateBoardAboutShipsAndMiss()
        {
            var ship1 = Substitute.For<IShip>();
            ship1.Segments = new List<Segment>()
            {
                new Segment(){Row = 1, Column = 0, SegmentState = SegmentState.Hit},
                new Segment(){Row = 1, Column = 1, SegmentState = SegmentState.Hit},
                new Segment(){Row = 1, Column = 2, SegmentState = SegmentState.Hit},
                new Segment(){Row = 1, Column = 3, SegmentState = SegmentState.Unknown}
            };
            var ship2 = Substitute.For<IShip>();
            ship2.Segments = new List<Segment>()
            {
                new Segment(){Row = 0, Column = 0, SegmentState = SegmentState.Sunk},
                new Segment(){Row = 0, Column = 1, SegmentState = SegmentState.Sunk},
                new Segment(){Row = 0, Column = 2, SegmentState = SegmentState.Sunk},
                new Segment(){Row = 0, Column = 3, SegmentState = SegmentState.Sunk}
            };
            _gameBoard.UpdateBoard(new List<IShip>(){ ship1, ship2 });

            _gameBoard.Board.Where(segment => segment.SegmentState == SegmentState.Sunk).Count().Should().Be(4);
            _gameBoard.Board.Where(segment => segment.SegmentState == SegmentState.Hit).Count().Should().Be(3);
            _gameBoard.Board.Where(segment => segment.SegmentState == SegmentState.Unknown).Count().Should().Be(93);
        }
    }
}
