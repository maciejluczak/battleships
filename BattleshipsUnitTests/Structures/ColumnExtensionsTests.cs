﻿using System;
using System.Collections.Generic;
using System.Text;
using Battleships.Structures;
using FluentAssertions;
using Xunit;

namespace BattleshipsUnitTests.Structures
{
    public class ColumnExtensionsTests
    {
        [Theory]
        [InlineData("A", 0)]
        [InlineData("B", 1)]
        [InlineData("C", 2)]
        [InlineData("D", 3)]
        [InlineData("E", 4)]
        [InlineData("F", 5)]
        [InlineData("G", 6)]
        [InlineData("H", 7)]
        [InlineData("I", 8)]
        [InlineData("J", 9)]
        public void ToColumn_ForColumnFormAToJ_ConvertStringToColumn(string column, int parsedColumn)
        {
            column.ToColumn().Should().Be(parsedColumn);
        }

        [Theory]
        [InlineData("a", 0)]
        [InlineData("j", 9)]
        public void ToColumn_ForLowerCase_ConvertStringToColumn(string column, int parsedColumn)
        {
            column.ToColumn().Should().Be(parsedColumn);
        }

        [Theory]
        [InlineData("k")]
        [InlineData("l")]
        [InlineData("")]
        [InlineData("AB")]
        public void ToColumn_ForInvalidString_ConvertStringToColumn(string column)
        {
            Assert.Throws<ArgumentOutOfRangeException>(()=>column.ToColumn());
        }
    }
}
