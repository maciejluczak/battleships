﻿using Battleships.Structures;
using System;
using System.Collections.Generic;
using System.Text;
using Battleships;
using Xunit;
using FluentAssertions;

namespace BattleshipsUnitTests
{
    public class InputValidatorTests
    {
        private readonly IInputValidator _inputValidator;
        public InputValidatorTests()
        {
            _inputValidator = new InputValidator();
        }

        [Theory]
        [InlineData("A1", 0, 0)]
        [InlineData("A10", 0, 9)]
        [InlineData("J1", 9, 0)]
        [InlineData("J10", 9, 9)]
        public void Validate_ForValidParmas_ReturnRowAndColumn(string command, int column, int row)
        {
            (var resultColumn, var resultRow) = _inputValidator.Validate(command);

            resultColumn.Should().Be(column);
            resultRow.Should().Be(row);
        }

        [Theory]
        [InlineData("A0")]
        [InlineData("A11")]
        [InlineData("K1")]
        [InlineData("J11")]
        public void Validate_OutOfRangeParams_ThrowExceptoion(string command)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _inputValidator.Validate(command));
        }
    }
}
