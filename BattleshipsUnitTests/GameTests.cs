using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Battleships;
using Battleships.ShipPlacement;
using Battleships.Structures;
using Battleships.io;
using FluentAssertions;
using NSubstitute;
using NSubstitute.ExceptionExtensions;

namespace BattleshipsUnitTests
{
    public class GameTests
    {
        private readonly IShooter _shooter;
        private readonly IInputValidator _validator;
        private readonly IOutput _output;
        private readonly IInput _input;
        private readonly IShipPlacement _shipPlacement;

        private readonly Game _game;
        
        public GameTests()
        {
            _shooter = Substitute.For<IShooter>();
            _validator = Substitute.For<IInputValidator>();
            _output = Substitute.For<IOutput>();
            _input = Substitute.For<IInput>();
            _shipPlacement = Substitute.For<IShipPlacement>();

            _game = new Game(_shipPlacement, _shooter, _output, _input, _validator);
        }

        [Fact]
        public void Game_WhenCreated_PrepareGameBoard10x10()
        {
            _game.Board.Count.Should().Be(100);
        }

        [Fact]
        public void Game_WhenCreated_FindsEmptyPlaceForShips()
        {
            _shipPlacement.FindEmptyPlaceForShip(
                Arg.Any<int>(), Arg.Any<List<IShip>>()).Returns(new List<Segment>());

            _shipPlacement.Received(1).FindEmptyPlaceForShip(5, Arg.Any<List<IShip>>());
            _shipPlacement.Received(2).FindEmptyPlaceForShip(4, Arg.Any<List<IShip>>());
        }

        [Fact]
        public void Play_WithInputSequenceSunkLastShip_ProccesOnlyOneIterationOfLoop()
        {
            string command = "A1";
            _input.GetInput().Returns(command);
            _validator.Validate(command)
                .Returns((0, 0));
            _shooter
                .Shoot(0, 0, Arg.Any<GameBoard>(), Arg.Any<List<IShip>>())
                .Returns((true, SegmentState.Sunk));

            _game.Play();

            _input.Received(2).GetInput();
            _output.Received(2).Draw(Arg.Any<List<Segment>>(), Arg.Any<string>());
            _output.Received(1).WriteWinMessage();
            _validator.Received(1).Validate(command);
            _shooter.Received(1).Shoot(0, 0, Arg.Any<GameBoard>(), Arg.Any<List<IShip>>());
        }

        [Fact]
        public void Play_WithInputSequenceMissHitSunkLastShip_ProccesThreeIterationOfLoop()
        {
            _input.GetInput().Returns("A1","A2","A3");
            _validator.Validate(Arg.Any<string>())
                .Returns((0, 0), (0, 1), (0, 2));
            _shooter
                .Shoot(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<GameBoard>(), Arg.Any<List<IShip>>())
                .Returns((false, SegmentState.Miss), (false, SegmentState.Hit), (true, SegmentState.Sunk));

            _game.Play();

            _input.Received(4).GetInput();
            _output.Received(4).Draw(Arg.Any<List<Segment>>(), Arg.Any<string>());
            _output.Received(1).WriteWinMessage();
            _validator.Received(3).Validate(Arg.Any<string>());
            _shooter.Received(3).Shoot(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<GameBoard>(), Arg.Any<List<IShip>>());
        }

        [Fact]
        public void Play_WithInputSequenceIncorectInputSunkLastShip_CallDrawWithErrorMessage()
        {
            string commandInvalid = "K1";
            string commandValid = "A1";
            _input.GetInput().Returns(commandInvalid, commandValid);
            _validator.Validate(commandInvalid)
                .Throws(new ArgumentOutOfRangeException());
            _validator.Validate(commandValid)
                .Returns((0, 0));
            _shooter
                .Shoot(0, 0, Arg.Any<GameBoard>(), Arg.Any<List<IShip>>())
                .Returns((true, SegmentState.Sunk));

            _game.Play();

            _input.Received(3).GetInput();
            _output.Received(3).Draw(Arg.Any<List<Segment>>(), Arg.Any<string>());
            _output.Received(1).Draw(Arg.Any<List<Segment>>(), new ArgumentOutOfRangeException().Message);
            _output.Received(1).WriteWinMessage();
            _validator.Received(2).Validate(Arg.Any<string>());
            _shooter.Received(1).Shoot(0, 0, Arg.Any<GameBoard>(), Arg.Any<List<IShip>>());
        }
    }
}
