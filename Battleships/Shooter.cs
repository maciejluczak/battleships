﻿using System.Collections.Generic;
using System.Linq;
using Battleships.Structures;

namespace Battleships
{
    public interface IShooter
    {
        (bool finish, SegmentState shotResult) Shoot(int column, int row, IGameBoard board, List<IShip> ships);
    }

    public class Shooter : IShooter
    {
        public (bool finish, SegmentState shotResult) Shoot(int column, int row, IGameBoard board, List<IShip> ships)
        {
            IShip shotShip = FindShip(column, row, ships);
            if (shotShip != null)
            {
                var shotResult = AddHit(column, row, shotShip, board);
                return (IsGameFinished(ships), shotResult);
            }
            else
            {
                AddMiss(column, row, board);
                return (false, SegmentState.Miss);
            }
        }

        private IShip FindShip(int column, int row, List<IShip> ships)
        {
            return ships.FirstOrDefault(ship =>
                ship.GetSegment(column, row) != null
            );
        }

        private SegmentState AddHit(int column, int row, IShip shotShip, IGameBoard board)
        {
            shotShip.GetSegment(column, row).SegmentState = SegmentState.Hit;
            bool sunk = shotShip.TrySink();
            board.UpdateBoard(new List<IShip>() { shotShip });

            if (sunk) return SegmentState.Sunk;
            else return SegmentState.Hit;
        }

        private void AddMiss(int column, int row, IGameBoard board)
        {
            board.AddMissShot(new Segment() { Column = column, Row = row, SegmentState = SegmentState.Miss });
            board.UpdateBoard(new List<IShip>());
        }

        private bool IsGameFinished(List<IShip> ships)
        {
            return ships.All(ship => ship.ShipState == ShipState.Sunk);
        }
    }
}
