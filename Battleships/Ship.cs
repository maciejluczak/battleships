﻿using System.Collections.Generic;
using System.Linq;
using Battleships.Structures;

namespace Battleships
{
    public interface IShip
    {
        int Size { get; }
        string Name { get; }
        ShipState ShipState { get; }
        List<Segment> Segments { get; set; }
        bool TrySink();
        Segment GetSegment(int column, int row);
    }

    public class Ship : IShip
    {
        public int Size { get; }
        public string Name { get; }
        public ShipState ShipState { get; private set; }
        public List<Segment> Segments { get; set; }

        public Ship(int size, string name)
        {
            Size = size;
            Name = name;
            ShipState = ShipState.Healthy;
        }

        public bool TrySink()
        {
            if (Segments.All(
                segment => segment.SegmentState == SegmentState.Hit || segment.SegmentState == SegmentState.Sunk))
            {
                Segments.ForEach(segment => segment.SegmentState = SegmentState.Sunk);
                ShipState = ShipState.Sunk;
                return true;
            }
            else
            {
                return false;
            }
        }

        public Segment GetSegment(int column, int row)
        {
            return Segments.FirstOrDefault(
                segment => segment.Row == row && segment.Column == column);
        }
    }
}
