﻿using Battleships.ShipPlacement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Battleships.Structures;
using Battleships.io;

namespace Battleships
{
    public class Game
    {
        private GameBoard _gameBoard;

        public List<Segment> Board
        {
            get { return _gameBoard.Board; }
        }

        private readonly IShipPlacement _shipPlacementStrategy;
        private readonly IShooter _shooter;
        private readonly IOutput _output;
        private readonly IInput _input;
        private readonly IInputValidator _inputValidator;
        private List<IShip> _shipList;

        public Game(IShipPlacement shipPlacementStrategy, IShooter shooter,
            IOutput output, IInput input, IInputValidator inputValidator)
        {
            _shipPlacementStrategy = shipPlacementStrategy;
            _shooter = shooter;
            _output = output;
            _input = input;
            _inputValidator = inputValidator;

            InitializeNewGame();
        }

        public void Play()
        {
            string message = "";
            SegmentState shotResult;
            bool gameFinished = false;
            while (!gameFinished)
            {
                _output.Draw(Board, message);
                var shotInput = _input.GetInput();
                try
                {
                    (gameFinished, shotResult) = MakeMove(shotInput);
                    message = shotResult.ToString();
                }
                catch (Exception e)
                {
                    message = e.Message;
                }
            }
            _output.Draw(Board, message);
            _output.WriteWinMessage();
            _input.GetInput();
        }

        private (bool finish, SegmentState result) MakeMove(String shotInput)
        {
            (int column, int row) = _inputValidator.Validate(shotInput);
            return _shooter.Shoot(column, row, _gameBoard, _shipList);
        }

        private void InitializeNewGame()
        {
            _gameBoard = new GameBoard();
            _shipList = new List<IShip>();

            var _battleship = new Ship(5, "Battleship");
            _battleship.Segments =
                _shipPlacementStrategy.FindEmptyPlaceForShip(_battleship.Size, _shipList);
            _shipList.Add(_battleship);

            var _destroyer = new Ship(4, "Destroyer");
            _destroyer.Segments =
                _shipPlacementStrategy.FindEmptyPlaceForShip(_destroyer.Size, _shipList);
            _shipList.Add(_destroyer);

            var _destroyer2 = new Ship(4, "Destroyer");
            _destroyer2.Segments =
                _shipPlacementStrategy.FindEmptyPlaceForShip(_destroyer2.Size, _shipList);
            _shipList.Add(_destroyer2);
        }
    }
}
