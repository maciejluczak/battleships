﻿namespace Battleships.Structures
{
    public class Segment
    {
        public int Column { get; set; }
        public int Row { get; set; }
        public SegmentState SegmentState { get; set; }
    }
}
