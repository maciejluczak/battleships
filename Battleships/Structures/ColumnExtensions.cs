﻿using System;

namespace Battleships.Structures
{
    public static class ColumnExtensions
    {
        public static int ToColumn(this string column)
        {
            column = column.ToUpper();
            if (column == "A") return 0;
            if (column == "B") return 1;
            if (column == "C") return 2;
            if (column == "D") return 3;
            if (column == "E") return 4;
            if (column == "F") return 5;
            if (column == "G") return 6;
            if (column == "H") return 7;
            if (column == "I") return 8;
            if (column == "J") return 9;
            throw new ArgumentOutOfRangeException();
        }
    }
}
