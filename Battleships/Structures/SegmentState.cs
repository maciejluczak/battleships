﻿namespace Battleships.Structures
{
    public enum SegmentState
    {
        Hit, Miss, Sunk, Unknown
    }
}