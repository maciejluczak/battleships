﻿namespace Battleships.Structures
{
    public enum ShipState
    {
        Healthy, Sunk
    }
}