﻿using System;
using System.Collections.Generic;
using System.Linq;
using Battleships.Structures;

namespace Battleships
{
    public interface IGameBoard
    {
        List<Segment> Board { get; }
        List<Segment> Miss { get; }

        void AddMissShot(Segment segment);
        void UpdateBoard(List<IShip> ships);
    }

    public class GameBoard : IGameBoard
    {
        private const int _boardSize = 10;
        public List<Segment> Board { get; private set; }
        public List<Segment> Miss { get; }

        public GameBoard()
        {
            CreateEmptyGameBoard();
            Miss = new List<Segment>();
        }

        private void CreateEmptyGameBoard()
        {
            Board = new List<Segment>();
            for (int rows = 0; rows < _boardSize; rows++)
            {
                for (int columns = 0; columns < _boardSize; columns++)
                {
                    Board.Add(new Segment(){Column = columns, Row = rows, SegmentState = SegmentState.Unknown});
                }
            }
        }

        public void UpdateBoard(List<IShip> ships)
        {
            ships.ForEach(ship =>
            {
                ship.Segments.ForEach(shipSegment =>
                {
                    if (shipSegment.SegmentState == SegmentState.Hit || shipSegment.SegmentState == SegmentState.Sunk)
                        Board.FirstOrDefault(
                            boardSegment => boardSegment.Column == shipSegment.Column && boardSegment.Row == shipSegment.Row)
                            .SegmentState = shipSegment.SegmentState;
                    else
                        Board.FirstOrDefault(
                            boardSegment => boardSegment.Column == shipSegment.Column && boardSegment.Row == shipSegment.Row)
                            .SegmentState = SegmentState.Unknown;
                });
            });
        }

        public void AddMissShot(Segment miss)
        {
            Miss.Add(miss);
            Board.FirstOrDefault(
                segment => segment.Column == miss.Column &&
                           segment.Row == miss.Row).SegmentState = SegmentState.Miss;
        }
    }
}