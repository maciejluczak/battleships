﻿using Battleships.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Battleships.io
{
    public interface IOutput
    {
        void Draw(List<Segment> board, string message);
        void WriteWinMessage();
    }

    public class ConsoleOutput : IOutput
    {
        public void Draw(List<Segment> board, string message)
        {
            Console.Clear();
            Console.Write(" ");
            for (int i = 0; i < 10; i++)
            {
                Console.Write(" " + (Column)i + " ");
            }
            Console.WriteLine();
            board
                .OrderBy(segment => segment.Row)
                .ThenBy(segment => segment.Column)
                .ToList()
                .ForEach(segment =>
                {
                    if (segment.Column == 0) Console.Write((segment.Row + 1) + " ");
                    if (segment.SegmentState == SegmentState.Unknown) Console.Write(" O ");
                    if (segment.SegmentState == SegmentState.Hit) Console.Write(" X ");
                    if (segment.SegmentState == SegmentState.Miss) Console.Write(" - ");
                    if (segment.SegmentState == SegmentState.Sunk) Console.Write(" $ ");
                    if ((segment.Column + 1) % 10 == 0 && segment.Column != 0) Console.WriteLine();
                });
            Console.WriteLine(message);
        }

        public void WriteWinMessage()
        {
            Console.WriteLine("You Won!!!!");
        }
    }
}
