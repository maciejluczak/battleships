﻿using System;

namespace Battleships.io
{
    public interface IInput
    {
        string GetInput();
    }

    public class ConsoleInput : IInput
    {
        public string GetInput()
        {
            return Console.ReadLine();
        }
    }
}
