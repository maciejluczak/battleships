﻿using System;
using System.Collections.Generic;
using System.Linq;
using Battleships.Structures;

namespace Battleships.ShipPlacement
{
    public interface IShipPlacement
    {
        List<Segment> FindEmptyPlaceForShip(int size, List<IShip> shipsOnBoard);
    }

    public class ShipPlacementRandom : IShipPlacement
    {
        private readonly Random _random;
        private const int _boardSize = 10;

        public ShipPlacementRandom(Random random)
        {
            _random = random;
        }

        public List<Segment> FindEmptyPlaceForShip(int size, List<IShip> shipsOnBoard)
        {
            List<Segment> segmentsRange = null;
            bool found = false;
            while (!found)
            {
                segmentsRange = RandomSegmentsRange(size);
                found = IsSegmentsRangeFree(shipsOnBoard, segmentsRange);
            }
            return segmentsRange;
        }

        private bool IsSegmentsRangeFree(List<IShip> ships, List<Segment> segmentsRange)
        {
            return segmentsRange.All(segment =>
                ships.Where(
                    ship => 
                        ship.Segments.Where(
                            shipSegment =>
                                shipSegment.Row == segment.Row && shipSegment.Column == segment.Column
                        ).Count() > 0 
                 ).Count() == 0
            );
        }

        private List<Segment> RandomSegmentsRange(int size)
        {
            var randomOrientation = _random.Next(100) % 2;
            var startRow = _random.Next(_boardSize - size);
            var endRow = startRow + size;
            var startColumn = _random.Next(_boardSize);
            var endColumn = startColumn;
            if (randomOrientation == 1)
            {
                var tmp = startRow;
                startRow = startColumn;
                startColumn = tmp;
                tmp = endColumn;
                endColumn = endRow;
                endRow = tmp;
            }
            List<Segment> segments = new List<Segment>();
            for (int row = startRow; row < endRow; row++)
            {
                segments.Add(new Segment()
                    { Column = startColumn, Row = row, SegmentState = SegmentState.Unknown });
            }
            for (int column = startColumn; column<endColumn; column++)
            {
                segments.Add(new Segment()
                    { Column = column, Row = startRow, SegmentState = SegmentState.Unknown });
            }

            return segments;
        }
    }
}
