﻿using System;
using Battleships.Structures;

namespace Battleships
{
    public interface IInputValidator
    {
        (int column, int row) Validate(string command);
    }

    public class InputValidator : IInputValidator
    {
        public (int column, int row) Validate(String command)
        {
            int column = command.Substring(0, 1).ToColumn();
            int row = Int32.Parse(command.Substring(1)) - 1;
            if (row > 9 || row < 0) throw new ArgumentOutOfRangeException();
            return (column, row);
        }
    }
}
