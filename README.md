# Battleships

## Description

Application to allow a single human player to play a one-sided game of Battleships against ships placed by the computer.
Program create 10x10 board and place 3 ships on it:

* 1x Battleship (5 squares)
* 2x Destroyers (4 squares)

Player makes a move by entering coordinates in form "A1", where “A” is the column and “5” is the row.      
The game ends when all ships are sunk. 

## Solution structure

Solution contains 3 projects:

* Battleships - core of the game logic
* BattleshipsUnitTest - unit tests for Battleships project
* BattleshipsConsoleApp - very basic console application, uses Battleship project, allow player to play game.

## Build and Run

Requirements:

* .net core 2.1 sdk
* git

To build and run BattleshipConsoleApp execute commands:

```sh
git clone https://bitbucket.org/maciejluczak/battleships
cd battleships
dotnet run -p BattleshipsConsoleApp
```

To build and run BattleshipsUnitTests project execute commands:

```sh
cd BattleshipsUnitTests
dotnet test -v=normal
```

There is also possible to open solution in Visual Studio 2017, then build and run application and tests from it.

## Problem of the random algorithm

Program places ship on game board by random. In case of three ships this is not a problem, this is easy to find free place. In case of add more ships to the game there is threat that random algorithm can never find free space. So in this situation the random algorithm should be extended or replaced by other.

