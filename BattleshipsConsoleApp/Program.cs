﻿using System;
using System.Data;
using System.Linq;
using Battleships;
using Battleships.ShipPlacement;
using Battleships.Structures;
using Battleships.io;

namespace BattleshipsConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game  = new Game(
                new ShipPlacementRandom(new Random()),
                new Shooter(),
                new ConsoleOutput(),
                new ConsoleInput(),
                new InputValidator());
            game.Play();
        }
    }
}
